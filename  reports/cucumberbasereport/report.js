$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/loginACME/Login.feature");
formatter.feature({
  "name": "Login ACME",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Positive flow",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open the Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "VendorName.openTheBrowser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Maximize the Browser",
  "keyword": "And "
});
formatter.match({
  "location": "VendorName.maximizeTheBrowser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter the URL",
  "keyword": "And "
});
formatter.match({
  "location": "VendorName.enterTheURL()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter the e-mail",
  "keyword": "And "
});
formatter.match({
  "location": "VendorName.enterTheEMail()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter the password",
  "keyword": "And "
});
formatter.match({
  "location": "VendorName.enterThePassword()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click Login",
  "keyword": "When "
});
formatter.match({
  "location": "VendorName.clickLogin()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Hover on Vendors",
  "keyword": "And "
});
formatter.match({
  "location": "VendorName.hoverOnVendors()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Search for vendor",
  "keyword": "When "
});
formatter.match({
  "location": "VendorName.clickOnSearchForVendor()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter the Vendor Taxid",
  "keyword": "And "
});
formatter.match({
  "location": "VendorName.enterTheVendorTaxid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Search",
  "keyword": "When "
});
formatter.match({
  "location": "VendorName.clickOnSearch()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify Vendor name is displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "VendorName.verifyVendorNameIsDisplayed()"
});
formatter.result({
  "status": "passed"
});
});