package loginACME;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class VendorName {
	ChromeDriver driver;
@Given("Open the Browser")
public void openTheBrowser() {
  System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
   driver=new ChromeDriver();
  
}

@Given("Maximize the Browser")
public void maximizeTheBrowser() {
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}

@Given("Enter the URL")
public void enterTheURL() {
driver.get("https://acme-test.uipath.com/account/login");    
}

@Given("Enter the e-mail")
public void enterTheEMail() {
	driver.findElementById("email").sendKeys("vshan2247@gmail.com");

}

@Given("Enter the password")
public void enterThePassword() {
	driver.findElementById("password").sendKeys("chocolate");
}

@When("click Login")
public void clickLogin() {
	driver.findElementById("buttonLogin").click();   
}

@When("Hover on Vendors")
public void hoverOnVendors() {
	Actions action=new Actions(driver);
    WebElement hover = driver.findElementByXPath("(//button[@class='btn btn-default btn-lg'])[4]");
     action.moveToElement(hover).perform();
        
}

@When("Click on Search for vendor")
public void clickOnSearchForVendor() {
	 driver.findElementByLinkText("Search for Vendor").click();   
}

@When("Enter the Vendor Taxid")
public void enterTheVendorTaxid() {
	 driver.findElementById("vendorTaxID").sendKeys("FR329083");
}

@When("Click on Search")
public void clickOnSearch() {
	  driver.findElementById("buttonSearch").click();
}

@Then("Verify Vendor name is displayed")
public void verifyVendorNameIsDisplayed() {
	 String name = driver.findElementByXPath("//table[@class= 'table']//tr[2]/td").getText();
	    System.out.println("The Vendor name is"+ name );
}



}
