package acme_account;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class LoginACME {
	@Test
	
	public void launchACME() throws InterruptedException {
	
		//launch browser
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		//launch url
		driver.get("https://acme-test.uipath.com/account/login");
		//enter username
		driver.findElementById("email").sendKeys("vshan2247@gmail.com");
		//enter password
		driver.findElementById("password").sendKeys("chocolate");
		//click login
		driver.findElementById("buttonLogin").click();
		//hover on vendor
		Thread.sleep(2000);
	    Actions action=new Actions(driver);
	    WebElement hover = driver.findElementByXPath("(//button[@class='btn btn-default btn-lg'])[4]");
         action.moveToElement(hover).perform();
	    
	    
	    driver.findElementByLinkText("Search for Vendor").click();
	    //enter taxid
	    driver.findElementById("vendorTaxID").sendKeys("FR329083");
	    //click search
	    driver.findElementById("buttonSearch").click();
	    Thread.sleep(3000);
	    //printname
	    String name = driver.findElementByXPath("//table[@class= 'table']//tr[2]/td").getText();
	    System.out.println("The Vendor name is"+ name );
	}   		
	}


