package week5.day1;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
@Test
public class PracticeReports {
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
 public void runReport() {
	 html=new ExtentHtmlReporter("./report/extentReport.html");
	 extent=new ExtentReports();
	 html.setAppendExisting(true);
	 extent.attachReporter(html);
	 test=extent.createTest("TC001_Login","Login into leaftaps");
	 test.assignAuthor("Shan");
	 test.assignCategory("Smoke");
	 try {
		 test.pass("Username DemoSalesManager entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		 }catch(IOException e) {
			 e.printStackTrace();
		 }
	 extent.flush();
 }


}
