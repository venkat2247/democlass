package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LaunchTestLeaf {
	public static void main(String[] args) {
		//launch browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		//maximize browser
		driver.manage().window().maximize();
		//load url
		driver.get("http://leaftaps.com/opentaps/");
		//enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//enter password
		driver.findElementById("password").sendKeys("crmsfa");
		//click login
		driver.findElementByClassName("decorativeSubmit").click();
		//click crm/sfa
		driver.findElementByLinkText("CRM/SFA").click();
		//click create lead
		driver.findElementByLinkText("Create Lead").click();
		//enter companyname
		driver.findElementById("createLeadForm_companyName").sendKeys("Perficient");
		//enter firstname
		driver.findElementById("createLeadForm_firstName").sendKeys("venkat");
		//enter lastname
		driver.findElementById("createLeadForm_lastName").sendKeys("shan");
		//driver.findElementByName("submitButton").click();
		//select dropdown
        WebElement eleSource = driver.findElementById("createLeadForm_dataSourceId");
        Select sel=new Select(eleSource);
        sel.selectByVisibleText("Employee");
        WebElement eleMarket = driver.findElementById("createLeadForm_marketingCampaignId");
        Select sel1=new Select(eleMarket);
        sel1.selectByValue("CATRQ_CARNDRIVER");
        WebElement eleIndustry = driver.findElementById("createLeadForm_industryEnumId");
        Select sel2 =new Select(eleIndustry);
        List<WebElement> ind = sel2.getOptions();
        for (WebElement test : ind) {
        	if(test.getText().startsWith("M")) {
        		System.out.println(test.getText());
        	}
			
		}
	}

}
