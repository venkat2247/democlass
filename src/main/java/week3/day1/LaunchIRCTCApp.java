package week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LaunchIRCTCApp {
	public static void main(String[] args) {
		//launch browser
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeDriver driver=new ChromeDriver();
				//maximize browser
				driver.manage().window().maximize();
				//load url
				driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
				//enter username
				driver.findElementById("userRegistrationForm:userName").sendKeys("Venki2247");
				//password
				driver.findElementById("userRegistrationForm:password").sendKeys("Virat@1234");
				//confirm password
				driver.findElementById("userRegistrationForm:confpasword").sendKeys("Virat@1234");
				//security question
				WebElement question = driver.findElementById("userRegistrationForm:securityQ");
				Select sel1=new Select(question);
				sel1.selectByIndex(5);
				//security answer
				driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("BCCI");
				//preferred language
				WebElement lan = driver.findElementById("userRegistrationForm:prelan");
			    Select sel2=new Select(lan);
			    sel2.selectByVisibleText("English");
			    //firstname
			    driver.findElementById("userRegistrationForm:firstName").sendKeys("Venkat");
			    //gender
			    driver.findElementById("userRegistrationForm:gender:0").click();
			    //marital status
			    driver.findElementById("userRegistrationForm:maritalStatus:1").click();
			    //DOB(day)
			    WebElement day = driver.findElementById("userRegistrationForm:dobDay");
	            Select sel3=new Select(day);
	            sel3.selectByIndex(03);
	            //DOB(month)
	            WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
	            Select sel4=new Select(month);
	            sel4.selectByVisibleText("JUN");
	            //DOB(year)
	            WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
	            Select sel5=new Select(year);
	            sel5.selectByVisibleText("1995");
	            //occupation
	            WebElement occ = driver.findElementById("userRegistrationForm:occupation");
	            Select sel6=new Select(occ);
	            sel6.selectByValue("2");
	            //country
	            WebElement cont = driver.findElementById("userRegistrationForm:countries");
	            Select sel7=new Select(cont);
	            sel7.selectByVisibleText("India");
	            //email
	            driver.findElementById("userRegistrationForm:email").sendKeys("venkat1234@gmail.com");
	            //mobno
	            driver.findElementById("userRegistrationForm:mobile").sendKeys("9894842848");
	            //nationality
	            WebElement identity = driver.findElementById("userRegistrationForm:nationalityId");
	            Select sel8=new Select(identity);
	            sel8.selectByVisibleText("India");
	            //doorno
	            driver.findElementById("userRegistrationForm:address").sendKeys("No.13,3rd Floor");
	            //pincode
	            driver.findElementById("userRegistrationForm:pincode").sendKeys("600024");
	            Thread.sleep(3000);
	             //city
	            
	            
	}

}
