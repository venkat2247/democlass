package week2.day2;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ExampleMap {
	public static void main(String[] args) {
		Map<Integer,String> map=new HashMap<>();
		map.put(1, "Gayatri");
		map.put(2, "Mohan");
		map.put(3,"Balaji");
		map.put(4, "Gopi");
		map.put(5,"Sarath");
		for(Entry<Integer,String> eachmap : map.entrySet()) {
		System.out.println(eachmap.getKey()+"--->"+eachmap.getValue());
		}
		
	}

}
