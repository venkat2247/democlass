package week4.day2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PracticeCommonMethods {
	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		
		
		
		
		
		
	
		
		
		Set<String>allwindows=driver.getWindowHandles();
		List<String> ls= new ArrayList<>();
		ls.addAll(allwindows);
		driver.switchTo().window(ls.get(0));
		
		//take snaps
		
		File src=driver.getScreenshotAs(OutputType.FILE);
		File des=new File("./snaps/img.png");
	    FileUtils.copyFile(src, des);	
	    
	   //alert 
	    driver.switchTo().alert().accept();
	    driver.findElementById(" ").sendKeys("shan");
	    driver.switchTo().alert().dismiss();
	    String text=driver.switchTo().alert().getText();
	    System.out.println("text");
	    
	    //frame
	    driver.switchTo().frame(2);
	    driver.switchTo().frame("Id or Name");
	    driver.switchTo().frame(driver.findElementById(""));
	    driver.switchTo().defaultContent();
	   //related to nested frame
	    driver.switchTo().parentFrame();
	    
	    //webtable
	    
	   WebElement table = driver.findElementById("Table 1");
	   List<WebElement> row = table.findElements(By.tagName("tr"));
	   List<WebElement> column = row.get(3).findElements(By.tagName("td"));
	   System.out.println(column.get(2).getText());
	    
	   //wait
	   
	  //implicitly wait
	   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	   //thread.sleep
	   Thread.sleep(2000);
	   //webdriver wait
	   WebElement element = driver.findElementById("");
	   WebDriverWait wait=new WebDriverWait(driver,10);
	   wait.until(ExpectedConditions.elementToBeClickable(driver.findElementById("")));
	   
	   //locators
	   
	   driver.findElementById("").clear();
	   driver.findElementById("").sendKeys("");
	   driver.findElementByXPath("").click();
	   driver.findElementsByClassName("").getAttribute();
	    
	    }

}
