package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class TestLeafHomework {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//enter password
		driver.findElementById("password").sendKeys("crmsfa");
		//click login
		driver.findElementByClassName("decorativeSubmit").click();
		//click crm/sfa
		driver.findElementByLinkText("CRM/SFA").click();
		//click create lead
		driver.findElementByLinkText("Create Lead").click();
		//click merge lead
		driver.findElementByLinkText("Merge Leads").click();
		//click lead icon
		driver.findElementByXPath("(//img[@alt = 'Lookup'])[1]").click();
		//moving to new window
		Set<String> allwindows=driver.getWindowHandles();
		List<String> ls=new ArrayList<String>();
		ls.addAll(allwindows);
		driver.switchTo().window(ls.get(1));
		//enter lead ID
		driver.findElementByName("id").sendKeys("10035");
		//click find leads
		driver.findElementByXPath("//button[text() = 'Find Leads']").click();
		driver.findElementByXPath("//td[@class ='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a").click();
		//click on icon near TO Lead
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		//move to new window
		allwindows=driver.getWindowHandles();
		ls=new ArrayList<String>();
		ls.addAll(allwindows);
		driver.switchTo().window(ls.get(2));
		//enter lead ID
		driver.findElementByName("id").sendKeys("10022");
		//click find leads button
		driver.findElementByXPath("//button[text() = 'Find Leads']").click();
		driver.findElementByXPath("//td[@class ='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a").click();
		//back to primary window
		driver.switchTo().window(ls.get(0));
		//click merge
		driver.findElementByLinkText("Merge").click();
		//accept alert
		driver.switchTo().alert().accept();
		//click findleads
		driver.findElementByLinkText("Find Leads").click();
		//enter lead ID
		driver.findElementByName("id").sendKeys("10035");
		//click findleads
		driver.findElementByXPath("//button[text() = 'Find Leads']").click();
		
		
		
		
		
	}

}
