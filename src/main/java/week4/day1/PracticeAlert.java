package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class PracticeAlert {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
	    driver.manage().window().maximize();
	    driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
	    driver.switchTo().frame("iframeResult");
	    driver.findElementByXPath("//button[text()='Try it']").click();
	    Alert myAlert = driver.switchTo().alert();
	    myAlert.sendKeys("shan");
	    myAlert.accept();
	}

}
