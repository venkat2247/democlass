package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class PracticeIRCTC {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");	
        ChromeDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElementByLinkText("Contact Us").click();
        Set<String> allwindows=driver.getWindowHandles();
        List<String> ls=new ArrayList<String>();
        ls.addAll(allwindows);
        //to get titlename
        driver.switchTo().window(ls.get(1));
        driver.getTitle();
        //to close base window
        driver.switchTo().window(ls.get(0));
        driver.close();
        
}
}